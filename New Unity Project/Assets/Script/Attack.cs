﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Attack : MonoBehaviour {

	GameObject Enemy;
	Button ATK;
	ChaosGuardian HP;
	Text HP_TEXT;

	private float timer = 0;
	public Vector2 pos = new Vector2 (0, 70f);
	public int Damage;

	void Awake () {

		ATK = GetComponent<Button>();
		Enemy = GameObject.Find("Enemy");
		HP = Enemy.GetComponentInChildren<ChaosGuardian> ();
		HP_TEXT = Enemy.GetComponentInChildren<Text> ();

		Debug.Log (pos);
	}

	// Use this for initialization
	void Start () {

		Damage = 50;

		ATK.onClick.AddListener (() => {
			TakeDamage ();
		});
			
	}

	public void TakeDamage () {
		
		HP.NOW_HP -= Damage;

		HP_TEXT.text = Damage.ToString();
		//HP_TEXT.rectTransform.Translate (0f,0.1f,0f);

		Debug.Log ("남은 체력 : " + HP.NOW_HP);
		//Debug.Log (HP_TEXT.rectTransform.anchoredPosition);


	}
		
	// Update is called once per frame
	void Update () {

		if (HP_TEXT.text == "50") {
		
			timer += Time.deltaTime;

			HP_TEXT.rectTransform.anchoredPosition = Vector2.Lerp (pos, new Vector2 (0, 120f), timer);


		}
	}
}
