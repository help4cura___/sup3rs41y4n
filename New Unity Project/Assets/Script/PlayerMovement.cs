﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float MovePower;
	public float JumpPower;

	Rigidbody2D rigid;
	Animator animator;
	SpriteRenderer render;

	bool isJumping = false;

	// Use this for initialization
	void Start () {
		rigid = gameObject.GetComponent<Rigidbody2D> () ;
		render = gameObject.GetComponentInChildren<SpriteRenderer> ();
		animator = gameObject.GetComponentInChildren<Animator> ();

		MovePower = 5f;
		JumpPower = 15f;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetAxisRaw ("Horizontal") == 0) {
			animator.SetBool ("isMoving", false);
		}
		else if (Input.GetAxisRaw ("Horizontal") < 0) { //Left
			animator.SetInteger ("moveHandler", -1);
			animator.SetBool ("isMoving", true);
		}
		else if (Input.GetAxisRaw ("Horizontal") > 0) { //Right
			animator.SetInteger ("moveHandler", 1);
			animator.SetBool ("isMoving", true);
		}
			
		if (Input.GetButtonDown ("Jump")) {
			
			isJumping = true;

		}
	}

	void FixedUpdate ()
	{
		Move ();
		Jump ();
	}

	void Move () {
		Vector3 moveVelocity = Vector3.zero;

		if (Input.GetAxisRaw ("Horizontal") < 0) {
			moveVelocity = Vector3.left;
			render.flipX = true;

		}

		else if (Input.GetAxisRaw ("Horizontal") > 0) {
			moveVelocity = Vector3.right;
			render.flipX = false;
		}

		transform.position += moveVelocity * MovePower * Time.deltaTime;
		}

	void Jump () {
		if(!isJumping)
			return;

		rigid.velocity = Vector2.zero;

		Vector2 jumpVelocity = new Vector2 (0, JumpPower);
		rigid.AddForce (jumpVelocity, ForceMode2D.Impulse);

		isJumping = false;

		}
}
