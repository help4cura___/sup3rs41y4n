﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthHandler : MonoBehaviour {

	GameObject Enemy;
	ChaosGuardian HP;
	Slider HP_Bar;

	public int MAX_HP, NOW_HP;

	// Use this for initialization
	void Start () {
		
		Enemy = GameObject.FindGameObjectWithTag ("Enemy");
		HP = Enemy.GetComponent<ChaosGuardian> ();

		HP_Bar = gameObject.GetComponentInChildren<Slider> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		MAX_HP = HP.MAX_HP;
		NOW_HP = HP.NOW_HP;

		HP_Bar.value = (float) NOW_HP / MAX_HP * 100; 

	}
}
